package com.nick.kstreams;

import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.TestCase;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.test.ProcessorTopologyTestDriver;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

public class TopologyWithGroupedTableTests extends TestCase {

    public StreamsConfig GetConfig() {
        UUID randomUuid = UUID.randomUUID();
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, randomUuid.toString());
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        StreamsConfig config = new StreamsConfig(props);
        return config;
    }

    @Test
    public void testSingleMessage() {
        String inputTopic = "input-topic";
        String outputTopic = "output-topic";
        StreamsConfig config = GetConfig();

        ApplicationConfig appConfig = new ApplicationConfig() {
            @Override
            public String sourceTopic() {
                return inputTopic;
            }

            @Override
            public String outputTopic() {
                return outputTopic;
            }
        };

        ObjectMapper oMapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        map.put("schema.registry.url", "");

        String inputKey = "key1";
        String inputVal = "theval";

        Deserializer<String> keyDeserializer = Serdes.String().deserializer();
        Deserializer<Long> valueDeserializer =
                Serdes.Long().deserializer();


        TopologyWithGroupedTable top = new TopologyWithGroupedTable(appConfig, map);
        Topology topology = top.get();
        ProcessorTopologyTestDriver driver = new ProcessorTopologyTestDriver(config, topology);
        driver.process(inputTopic, inputKey, inputVal, Serdes.String().serializer(), Serdes.String().serializer());

        ProducerRecord<String, Long> outputRecord = driver.readOutput(outputTopic, keyDeserializer, valueDeserializer);
        assertEquals("key1", outputRecord.key());
        assertEquals(Long.valueOf(1L), outputRecord.value());

    }

    public void testSeveralMessagesWithDifferentKeys() {
        String inputTopic = "input-topic";
        String outputTopic = "output-topic";
        StreamsConfig config = GetConfig();

        ApplicationConfig appConfig = new ApplicationConfig() {
            @Override
            public String sourceTopic() {
                return inputTopic;
            }

            @Override
            public String outputTopic() {
                return outputTopic;
            }
        };

        ObjectMapper oMapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        map.put("schema.registry.url", "");

        String inputKey = "key1";
        String inputVal = "theval";

        Deserializer<String> keyDeserializer = Serdes.String().deserializer();
        Deserializer<Long> valueDeserializer =
                Serdes.Long().deserializer();


        TopologyWithGroupedTable top = new TopologyWithGroupedTable(appConfig, map);
        Topology topology = top.get();
        ProcessorTopologyTestDriver driver = new ProcessorTopologyTestDriver(config, topology);
        driver.process(inputTopic, "key1", "theval", Serdes.String().serializer(), Serdes.String().serializer());
        driver.process(inputTopic, "key2", "theval", Serdes.String().serializer(), Serdes.String().serializer());
        driver.process(inputTopic, "key3", "theval", Serdes.String().serializer(), Serdes.String().serializer());

        ProducerRecord<String, Long> outputRecord = driver.readOutput(outputTopic, keyDeserializer, valueDeserializer);
        assertEquals("key1", outputRecord.key());
        assertEquals(Long.valueOf(1L), outputRecord.value());
        outputRecord = driver.readOutput(outputTopic, keyDeserializer, valueDeserializer);
        assertEquals("key2", outputRecord.key());
        assertEquals(Long.valueOf(1L), outputRecord.value());
        outputRecord = driver.readOutput(outputTopic, keyDeserializer, valueDeserializer);
        assertEquals("key3", outputRecord.key());
        assertEquals(Long.valueOf(1L), outputRecord.value());

    }

    public void testSeveralMessagesWithSameKeys() {
        String inputTopic = "input-topic";
        String outputTopic = "output-topic";
        StreamsConfig config = GetConfig();

        ApplicationConfig appConfig = new ApplicationConfig() {
            @Override
            public String sourceTopic() {
                return inputTopic;
            }

            @Override
            public String outputTopic() {
                return outputTopic;
            }
        };

        ObjectMapper oMapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        map.put("schema.registry.url", "");

        String inputKey = "key1";
        String inputVal = "theval";

        Deserializer<String> keyDeserializer = Serdes.String().deserializer();
        Deserializer<Long> valueDeserializer =
                Serdes.Long().deserializer();


        TopologyWithGroupedTable top = new TopologyWithGroupedTable(appConfig, map);
        Topology topology = top.get();
        ProcessorTopologyTestDriver driver = new ProcessorTopologyTestDriver(config, topology);
        driver.process(inputTopic, "key1", "theval", Serdes.String().serializer(), Serdes.String().serializer());
        driver.process(inputTopic, "key2", "theval", Serdes.String().serializer(), Serdes.String().serializer());
        driver.process(inputTopic, "key1", "theval", Serdes.String().serializer(), Serdes.String().serializer());

        ProducerRecord<String, Long> outputRecord = driver.readOutput(outputTopic, keyDeserializer, valueDeserializer);
        assertEquals("key1", outputRecord.key());
        assertEquals(Long.valueOf(1L), outputRecord.value());
        outputRecord = driver.readOutput(outputTopic, keyDeserializer, valueDeserializer);
        assertEquals("key2", outputRecord.key());
        assertEquals(Long.valueOf(1L), outputRecord.value());
        outputRecord = driver.readOutput(outputTopic, keyDeserializer, valueDeserializer);
        assertEquals("key1", outputRecord.key());
        assertEquals(Long.valueOf(1L), outputRecord.value()); //this fails, I get 0.  If I pull another message, it shows key1 with a count of 1

    }
}
