package com.nick.kstreams;

public interface ApplicationConfig {

    /**
     * Source topic name.
     */
    String sourceTopic();

    /**
     * Destination topic name.
     */
    String outputTopic();

}
