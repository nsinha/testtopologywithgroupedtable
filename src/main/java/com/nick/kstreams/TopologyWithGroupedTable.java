package com.nick.kstreams;
import org.apache.kafka.common.serialization.*;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import java.util.Map;


public class TopologyWithGroupedTable {

    private final Map<String, String> serdesConfig;

    private final ApplicationConfig applicationConfig;

    public TopologyWithGroupedTable(ApplicationConfig applicationConfig, Map<String, String> serdesConfig) {

        this.applicationConfig = applicationConfig;
        this.serdesConfig = serdesConfig;
    }

    public Topology get() {

        final StreamsBuilder builder = new StreamsBuilder();
        KGroupedTable<String, String> groupedTable
                = builder.table(applicationConfig.sourceTopic(), Consumed.with(Serdes.String(), Serdes.String()))
                .groupBy((key, value) -> KeyValue.pair(key, value), Serialized.with(Serdes.String(), Serdes.String()));

        KTable<String, Long> countTable = groupedTable.count();

        KStream<String, Long> countTableAsStream = countTable.toStream();
        countTableAsStream.to(applicationConfig.outputTopic(), Produced.with(Serdes.String(), Serdes.Long()));

        return builder.build();
    }
}